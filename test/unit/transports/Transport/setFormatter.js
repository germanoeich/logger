'use strict';

const { Transport } = require('../../../../build/transports/Transport');
const { LogLevel } = require('../../../../build/constants/LogLevel');

describe('#setFormatter', function() {
	const getClean = () => {
		const tested = new Transport();

		return tested;
	};

	it('should set the level and clear the known loggers map', function() {
		const tested = getClean();

		tested.knownLoggers.set('test', 'logger');

		tested.setLevel(LogLevel.DEBUG);

		expect(tested.options.level, 'to be', LogLevel.DEBUG);
		expect(tested.knownLoggers.size, 'to be', 0);
	});

	it('should use the default level INFO if no level is given', function() {
		const tested = getClean();

		tested.setLevel();

		expect(tested.options.level, 'to be', LogLevel.INFO);
	});
});
