import { Logger } from './Logger';

/**
 * Alias for {@link Logger.get}.
 *
 * @see {@link Logger.get}
 */
const get = Logger.get;
export { Logger, get };
export default Logger;

export { LogMeta } from './LogMeta';

export * from './constants';
export * from './formatter';
export * from './transports';
